from setuptools import find_packages, setup

package_name = 'custom_plugin'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name + '/resource', ['resource/CustomPlugin.ui']),
        ('share/' + package_name, ['package.xml']),
        ('share/' + package_name,  ['plugin.xml']),
        ('lib/' + package_name, ['scripts/custom_plugin']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='joe',
    maintainer_email='josephagrane@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    scripts=['scripts/custom_plugin'],
)
