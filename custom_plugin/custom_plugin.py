from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget
from rqt_gui_py.plugin import Plugin
import os
from ament_index_python import get_package_share_directory

class CustomPlugin(Plugin):
    
    def __init__(self, context):
        super(CustomPlugin, self).__init__(context)
        self.setObjectName("CustomPlugin")
        share_path = get_package_share_directory("custom_plugin")
        ui_path = os.path.join(share_path, "resource", "CustomPlugin.ui")
        self.widget = QWidget()
        loadUi(ui_path, self.widget)
        if context.serial_number() > 1:
            self.widget.setWindowTitle(
                self.widget.windowTitle() + f"({context.serial_number()})"
            )
        context.add_widget(self.widget)
        