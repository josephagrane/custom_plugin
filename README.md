# How to access the plugin

1. cd into the root directory of this repository
2. run the following command to build the code `colcon build`
3. source the install script to activate the new code `source install/setup.bash`
4. now, launch the eufs simulator to start up rqt `ros2 launch eufs_launcher eufs_launcher.launch.py`
5. in the launcher window, click launch, you can untick 'rviz gui' to make it launch faster
6. after a few seconds, the rqt window should open up, you can access the plugin by selecting 'plugins', then 'robot tools' and then 'custom plugin'

